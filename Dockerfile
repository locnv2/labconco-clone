FROM node:12-alpine as build

COPY package.json yarn.lock ./
COPY . .

RUN yarn
RUN yarn build

FROM node:12-alpine AS node_modules

COPY package.json yarn.lock ./

RUN yarn
RUN npm prune --production

FROM node:12-alpine

RUN mkdir -p /usr/src/app
RUN yarn global add serve

WORKDIR /usr/src/app

COPY --from=build build /usr/src/app/build
COPY --from=node_modules node_modules /usr/src/app/node_modules

COPY . ./usr/src/app

CMD ["serve", "-s", "build"]