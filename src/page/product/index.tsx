/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { default as Collapse } from 'antd/es/collapse'
import 'antd/es/collapse/style/index.css'
import { default as React } from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import DetailView from '../../components/detail-view'
import ListView from '../../components/list-view'
import { SLUG_URL } from '../../constants/slug'

interface Product {
    id?: number | string
    name?: string
    description?: string
    image?: string
    feature?: string
    note?: string
    referenceLink?: string
}

interface Catalog {
    id?: number | string
    name?: string
    image?: string
    catalog?: number
    attribute?: Attribute[]
    value?: Value[]
}
type Attribute = {
    id: number
    name: string
}
type Value = {
    id: number
    name: string
    attribute: Attribute
}
const { Panel } = Collapse

const PRODUCT_CATALOG_API = `products/catalog`

function ListCatalogView(listData: Catalog) {
    return (
        <div className='cell' key={listData.id}>
            <div className='product-thumb-2 subproduct'>
                <h5>
                    <Link to={`/catalog/${SLUG_URL(listData.name || '')}/${listData.catalog}`}>{listData.name}</Link>
                </h5>
                <div className='grid-x'>
                    <div className='shrink cell'>
                        <Link to={`/catalog/${SLUG_URL(listData.name || '')}/${listData.catalog}`}>
                            <img alt='' src={listData.image?.split(',')[0]} />
                        </Link>
                    </div>
                    <div className='auto cell'>
                        <p>
                            <b>Catalog # </b>
                            {listData.catalog}
                        </p>
                        <div className='product-comparison-check show-for-medium'>
                            <input
                                className='product-comparison-checkbox'
                                id='compare-product-1109'
                                type='checkbox'
                                defaultValue={1109}
                            />
                            <label htmlFor='compare-product-1109' className='compare-product'>
                                Compare
                            </label>
                        </div>
                    </div>
                </div>
                <ul>
                    {listData.value?.map((value: Value) => {
                        console.log(value.name)

                        return (
                            <li key={value.id}>
                                <strong>{value.name && value.attribute.name + ':'} </strong> {value.name}
                            </li>
                        )
                    })}
                </ul>
            </div>
        </div>
    )
}

function ProductView(listData: Product) {
    const getImage = listData.image?.split(',') || []

    return (
        <>
            <section className='article' id='product-facets'>
                <div className='grid-x grid-margin-x' id='faceted_search_for_master'>
                    <section id='product-title' className='article'>
                        <div className='grid-x grid-margin-x'>
                            <div className='large-6 cell c-product-images'>
                                <div className='c-product-images__lg'>
                                    <img
                                        alt='Paramount Ductless Enclosures'
                                        src={getImage[0]}
                                        title='Paramount Ductless Enclosures'
                                        className='js-main-img  '
                                        data-index={0}
                                    />
                                </div>
                                <ul
                                    className='c-product-thumbnails js-magnific-popup js-slick slick-initialized slick-slider'
                                    data-slick='{"slidesToShow": 4, "slidesToScroll": 4, "arrows" : true, "responsive":  [{"breakpoint":600, "settings":{"slidesToShow":1,"slidesToScroll":1}} ]}'>
                                    <div aria-live='polite' className='slick-list draggable'>
                                        <div className='slick-track' role='listbox' style={{ opacity: 1, width: '1386px' }}>
                                            {getImage?.map((image: string, index: number) => {
                                                return (
                                                    <li
                                                        className='c-product-thumbnails__img js-swap-img slick-slide slick-cloned'
                                                        data-index={4}
                                                        data-src='https://www.labconco.com/images/cms/extralarge/clrpmt25.jpg'
                                                        data-slick-index={-2}
                                                        aria-hidden='true'
                                                        tabIndex={-1}
                                                        style={{ width: '89px' }}
                                                        key={index}>
                                                        <a href={image} tabIndex={-1}>
                                                            <img
                                                                src={image}
                                                                style={{ width: '89px', height: '80px' }}
                                                                alt='Paramount Ductless Enclosures'
                                                            />
                                                        </a>
                                                    </li>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </ul>
                            </div>
                            <div className='large-6 cell'>
                                <h1 className='c-product-title'>{listData.name}</h1>
                                {ReactHtmlParser(listData.description || '')}
                            </div>
                        </div>
                    </section>
                    <section className='article largetext article--margin' id='product-description'>
                        <div className='grid-x'>
                            <div className='large-8 large-push-2 cell'>{listData.note}</div>
                        </div>
                    </section>
                </div>
            </section>
            <section className='article' id='product-long-description'>
                <Collapse accordion bordered={false} ghost={true} className='site-collapse-custom-collapse'>
                    <Panel
                        header={
                            <div className='grid-x grid-margin-x'>
                                <div className='auto cell'>
                                    <hr />
                                </div>
                                <div className='shrink cell text-center'>
                                    <button
                                        className='button opener'
                                        data-toggle='long-description-content description-opener-icon'>
                                        Additional Features &amp; Specifications
                                        <i
                                            className='fas fa-chevron-circle-down'
                                            id='description-opener-icon'
                                            data-toggler='fa-chevron-circle-up fa-chevron-circle-down'
                                            aria-expanded='true'
                                            data-e='4krtmx-e'
                                        />
                                    </button>
                                </div>
                                <div className='auto cell'>
                                    <hr />
                                </div>
                            </div>
                        }
                        key='id'>
                        <div id='long-description-content' className=''>
                            <div className='grid-x'>
                                <div className='large-8 large-push-2 cell'>{ReactHtmlParser(listData.feature || '')}</div>
                            </div>
                        </div>
                    </Panel>
                </Collapse>
            </section>
            <section className='article' id='product-facets'>
                <ListView urlApi={PRODUCT_CATALOG_API} listView={ListCatalogView} />
            </section>

            <section className='article  c-videos'>
                <div className='grid-x align-center'>
                    <div className='large-8  cell text-center '>
                        <div className='responsive-youtube'>
                            <iframe
                                id='ytIframe'
                                width={560}
                                height={315}
                                src={listData.referenceLink}
                                frameBorder={0}
                                allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
                                allowFullScreen
                            />
                        </div>
                    </div>
                    <div className='large-12 cell'></div>
                </div>
            </section>
        </>
    )
}

export default function ProductPage() {
    const PRODUCT_API = `products`
    return <DetailView urlApi={PRODUCT_API} detailView={ProductView} />
}
