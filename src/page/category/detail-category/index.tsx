/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/role-has-required-aria-props */
/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable no-unreachable */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/no-redundant-roles */
/* eslint-disable react-hooks/exhaustive-deps */
import { default as React, Fragment, useEffect, useState } from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Link, useHistory, useParams } from 'react-router-dom'
import api from '../../../constants/api'
import { SLUG_URL } from '../../../constants/slug'
import './style.scss'
interface Path {
    id: string
}

interface Product {
    id: number
    name: string
    image: string
    note?: string
}

interface SubCategories {
    id: number
    name: string
    products: Product[]
}

interface Category {
    id: number
    name: string
    image: string
    description: string
    country: string
    referenceLink: string
    subCategories: SubCategories[]
    note: string
}

export default function DetailCategoryPage() {
    const [category, setCategory] = useState<Category>()
    const [listCategory, setListCategory] = useState<any>()
    const path: Path = useParams()
    const history = useHistory()

    useEffect(() => {
        api.get('categories').then((res) => {
            setListCategory(res.data.data.data)
        })
        api.get(`categories/${path.id}`).then((res) => {
            setCategory(res.data.data.data)
        })
    }, [history.location])

    const getProducts = (products: Product[]) => {
        return products.map((item: Product) => {
            return (
                <div className='c-product-thumb slick-slide slick-cloned category-thumb-swoosh' key={item.id}>
                    <Link to={`/products/${SLUG_URL(item.name)}/${item.id}`}>
                        <img src={item.image.split(',')[0]} style={{ height: '400px' }} />
                        <h4 className='c-product-thumb__title'>{item.name}</h4>
                        <p>{item.note}</p>
                    </Link>
                </div>
            )
        })
    }

    const getSubCategories = () => {
        return (
            <div className='grid-x grid-margin-x'>
                {category?.subCategories.map((item: SubCategories) => {
                    return (
                        <div className='cell' key={item.id}>
                            <h2>{item.name}</h2>
                            <div className='large-12 cell js-slick slick-initialized slick-slider'>
                                <div className='slick-list draggable'>
                                    <div className='slick-track'>{getProducts(item.products)}</div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }
    return (
        <Fragment>
            <section id='category-title' className='article light-gray'>
                <div className='grid-x grid-margin-x'>
                    <div className='large-6 cell'>
                        <h1>{category?.name}</h1>
                        <p>{ReactHtmlParser(category?.description || '')}</p>
                        <p>{ReactHtmlParser(category?.note || '')}</p>
                    </div>
                    <div className='large-6 cell'>
                        {' '}
                        <img alt='Freeze Dryers' src={category?.image.split(',')[0]} />
                    </div>
                </div>
            </section>
            {/* Quick Link */}
            <section className='article article--margin'>
                <div
                    className='grid-x grid-padding-x'
                    data-equalizer=''
                    data-resize='dtpdl7-eq'
                    data-mutate='gh2oz1-eq'
                    data-e='8f19l9-e'>
                    <div className='large-6 cell has-border' data-equalize-watch=''>
                        <h4>Quick Links</h4>
                        <ul className='c-quick-links'>
                            {listCategory?.map((item: any, index: number) => {
                                return (
                                    <Link to={`/category/${item.name}/${item.id}`}>
                                        <li key={index}>{item.name}</li>
                                    </Link>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            </section>
            {/* Show application */}
            <section className='c-categories article'>{getSubCategories()}</section>
            {/* Video */}
            <section className='article  c-videos'>
                <div className='grid-x align-center'>
                    <div className='large-8  cell text-center '>
                        {/* video src */}
                        <div className='responsive-youtube'>
                            <iframe
                                id='ytIframe'
                                width={560}
                                height={315}
                                src={category?.referenceLink}
                                frameBorder={0}
                                allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
                                allowFullScreen
                            />
                        </div>
                        <div className='c-videos__desc js-desc text-left is-hidden js-desc--1'>
                            <h4>Protector XStream Chemical Fume Hood</h4>
                            <p>
                                This video explains the key features of the Protector XStream Chemical Fume Hood from Labconco,
                                yielding the greatest possible laboratory safety and lowest operating cost.
                            </p>
                        </div>
                        <div className='c-videos__desc js-desc text-left is-hidden js-desc--2'>
                            <h4>Webinar on Managing Lab Air with Chemical Fume Hoods</h4>
                            <p>
                                For many laboratories, managing air quality through engineered controls is essential for worker
                                safety and comfort. However, with numerous options available for fume extraction and air
                                purification, deciding on an engineered solution that best suits your lab’s current and future
                                needs is not always straightforward.&nbsp;
                            </p>
                        </div>
                        <div className='c-videos__desc js-desc text-left is-hidden js-desc--3'>
                            <h4>High Efficiency Fume Hood Airflow Demonstrations</h4>
                            <p>
                                See airflow demonstrations using smoke to show some of the innovative and highly effective
                                features that make the Protector XStream® the most effective containing fume hood. Watch the
                                airfoil sill, upper dilution air supply, sash handle and sash track direct air into the fume
                                hood's baffles without causing dangerous, containment disrupting turbulence.
                            </p>
                        </div>
                        <div className='c-videos__desc js-desc text-left is-hidden js-desc--4'>
                            <h4>Fume Hoods Overview 2013</h4>
                        </div>
                        <div className='c-videos__desc js-desc text-left is-hidden js-desc--5'>
                            <h4>Meet Manny (ASHRAE 110 Testing)</h4>
                            <p>
                                Being an ASHRAE 110 testing mannequin is a dirty job, but some dummy's got to do it. Manny is just
                                the guy for the job. Check out this video to get an idea of how we test our fume hoods to ensure
                                they provide proper protection.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}
