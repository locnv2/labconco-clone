import { default as React, Fragment, useEffect, useState } from 'react'
import ReactHtmlParser from 'react-html-parser'
import { Link } from 'react-router-dom'
import api from '../../constants/api'
import './style.scss'

export default function CategoryPage() {
    const [category, setCategory] = useState<any>()

    useEffect(() => {
        api.get('categories').then((res) => {
            setCategory(res.data.data.data)
        })
    }, [])

    return (
        <Fragment>
            <div className='article'>
                <h1>Products</h1>
                <div className='category_description'>
                    <p>
                        Labconco is dedicated to building superior laboratory equipment: Fume Hoods, Biosafety Cabinets, Balance
                        Enclosures, Glassware Washers, Water Purifiers, Glove Boxes, Forensic Enclosures, Freeze Dryers, and
                        Accessories.
                    </p>
                    <h3>Protecting people since 1925</h3>
                </div>
                <section id='category'>
                    <div className='grid-x grid-margin-x small-up-1 medium-up-2 large-up-4' style={{ marginTop: '40px' }}>
                        {category?.map((item: any, index: number) => {
                            return (
                                <div className='cell' key={index}>
                                    <div>
                                        <Link to={`/category/${item.name}/${item.id}`}>
                                            <img alt='Rotary Vane Direct Drive Vacuum Pump' src={item.image.split(',')[0]} />
                                        </Link>
                                        <h4>
                                            <Link to={`/category/${item.name}/${item.id}`}>{item.name}</Link>
                                        </h4>
                                        <p>{ReactHtmlParser(item.note || '')}</p>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </section>
            </div>
        </Fragment>
    )
}
