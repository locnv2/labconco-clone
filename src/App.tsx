import { default as React } from 'react'
import { BrowserRouter } from 'react-router-dom'
import "./assets/css/foundation-datepicker.css"
import "./assets/css/printc619.css"
import { MainRoutes } from './routes'

function App() {
    return (
        <BrowserRouter>
            {/* <MainLayout>
                <MainRoutes />
            </MainLayout> */}
            <MainRoutes />
        </BrowserRouter>
    );
}

export default App;
