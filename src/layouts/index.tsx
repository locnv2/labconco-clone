import { default as React, ReactNode } from 'react'
import { MainBreadcrumb } from './breadcrumb'
import FooterLayout from './footer'
import HeaderLayout from './header'
import './style.scss'

export function MainLayout({ children }: { children: ReactNode }) {
    return (
        <div style={{ position: 'relative', minHeight: '100%', top: '0' }}>
            <div id='sticky-layout'>
                <HeaderLayout />
                <div className='stripe '>
                    {/* <div className='row'>
                        <MainBreadcrumb />
                        {children}
                    </div> */}
                    
                        <MainBreadcrumb />
                        {children}
                
                </div>
            </div>
            <FooterLayout />
        </div>
    )
}
