import React from 'react'

export default function BannerLayout() {
    return (
        <section id='promo-carousel'>
            <div
                className='orbit row collapse'
                role='region'
                aria-label='Labconco promotions'
                data-orbit
                data-auto-play='true'
                data-timer-delay={10000}
                data-options='animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;'
                data-resize='xcyod0-orbit'
                id='xcyod0-orbit'
                data-e='s6jjs7-e'
                data-events='resize'>
                <div className='orbit-wrapper columns'>
                    <div className='orbit-controls'>
                        <button className='orbit-previous' tabIndex={0}>
                            <span className='show-for-sr'>Previous Slide</span>
                            <i className='material-icons'>chevron_left</i>
                        </button>
                        <button className='orbit-next' tabIndex={0}>
                            <span className='show-for-sr'>Next Slide</span>
                            <i className='material-icons'>chevron_right</i>
                        </button>
                    </div>
                    <ul className='orbit-container text-center' tabIndex={0} style={{ height: '484.719px' }}>
                        <li
                            className='orbit-slide fade-out mui-leave mui-leave-active'
                            data-slide={0}
                            style={{ position: 'relative', top: '0px', display: 'none' }}
                            aria-live='polite'>
                            <a href='/architects?utm_source=homepageutm_medium=bannerutm_campaign=3-AE Market Site'>
                                <p>
                                    <img
                                        alt='Browse our new site for lab planning professionals'
                                        src='https://www.labconco.com/images/cms/content/AESite_20210209.jpg'
                                    />
                                </p>
                            </a>
                        </li>
                        <li
                            className='orbit-slide is-active'
                            data-slide={1}
                            style={{ position: 'relative', top: '0px', display: 'block' }}
                            aria-live='polite'>
                            <a href='/washers?utm_source=homepageutm_medium=bannerutm_campaign=3-Glassware Washer Versatility Banner'>
                                <p>
                                    <img alt='' src='https://www.labconco.com/images/cms/content/washer-banner-v6.jpg' />
                                </p>
                            </a>
                        </li>
                        <li className='orbit-slide' data-slide={2} style={{ position: 'relative', top: '0px', display: 'none' }}>
                            <a href='https://www.labconco.com/services/xpress-inventory?utm_source=homepageutm_medium=bannerutm_campaign=4-48-Hour-XPress'>
                                <p>
                                    <img
                                        alt=''
                                        src='https://www.labconco.com/services/xpress-inventory?utm_source=homepageutm_medium=bannerutm_campaign=1-48-Hour-XPress'
                                    />
                                    <img alt='' src='https://www.labconco.com/images/cms/files/Xpress-Shipping%20Banner_2000%20x%20651_20210825.jpeg' />
                                </p>
                            </a>
                        </li>
                        <li className='orbit-slide' data-slide={3} style={{ position: 'relative', top: '0px', display: 'none' }}>
                            <a href='/vue?utm_source=homepageutm_medium=bannerutm_campaign=4-Go Big on Biosafety-Logic Vue Banner'>
                                <p>
                                    <img
                                        alt='New Logic Vue Large Class II Safety Enclosure'
                                        src='https://www.labconco.com/images/cms/content/logic-vue-banner-v6.png'
                                    />
                                </p>
                            </a>
                        </li>
                        <li className='orbit-slide' data-slide={4} style={{ position: 'relative', top: '0px', display: 'none' }}>
                            <a href='https://my.labconco.com/bsc-interest-ebook?utm_source=homepageutm_medium=bannerutm_campaign=5-Free BSC ebook'>
                                <p>
                                    <img
                                        alt='Biosafety cabinet safety knowledge - lab equipment'
                                        src='https://www.labconco.com/images/cms/content/labconco-bsc-ebook-banner.jpg'
                                    />
                                </p>
                            </a>
                        </li>
                    </ul>{' '}
                </div>
            </div>
        </section>
    )
}
